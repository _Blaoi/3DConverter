import os
import read3DFormat
import write3DFormat

formats = [".obj", ".ply"]
extension = ".ply"
file = "toto"


def print_formats():
    """Print all the 3D formats handled by the converter."""
    for f in range(0, len(formats)):
        print(f + 1, formats[f])


def pick_format():
    """Let the user pick a 3D Format and restart over while the format is choice is wrong."""
    i = int(input())
    while i < 1 or i > len(formats):
        i = int(input("Pick a valid format only !\n"))
    return i


def pick_file():
    """Allow the user to pick the file from the conversion."""


def read_format():
    """Read and store all the data from a file according an extension."""
    if "ply" in extension:
        read3DFormat.read_ply(file)
    elif extension is ".obj":
        read3DFormat.read_obj(file)


def write_format():
    """Write all the data into a file with a certain extension."""


def print_menu():
    """Display a menu that allow the user to choose the destination and source file formats."""
    print("== 3D Format Converter ==")
    print("Pick a source format")
    print_formats()
    format_source = pick_format()
    print("Format source", format_source)
    print("Pick a destination format")
    print_formats()
    format_destination = pick_format()
    print("Format destination", format_destination)
    read_format()
    write_format()
try:
    write3DFormat.write_ply('toto')
except OSError:
    print(OSError)
