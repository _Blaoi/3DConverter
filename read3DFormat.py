"""
This module contains method that will read in a certain 3D file.

The formats handled for the moments are:
    .obj
    .ply
The function will read the file (or throw an exception if it's unreadable) and store the data
into a dictionary. Read the documentation from the functions to know more about it.
    author: _Blaoi
    contact: blaoi@tuta.io
    Please contact me if any links/method doesn't work properly ! Don't be shy ;)
"""


def read_ply(file):
    """
    Attempt to read from a .ply file and store the data into a dictionary.

    :keyword file The file to read.

    If the file is not readable an exception is thrown.
    The main reasons of an unreadable file are:
        -File doesn't exist (maybe you have misspelled it or the path is wrong)
        -Impossible to read the file (You or this program doesn't have permission to read from this file)

    The format of the ply file must respect some rules that you can find here:
        http://bored-dev.org/toto

    :return dictionary A dictionary where are stored all the 3D data.
    """
    dictionary = dict()
    return dictionary


def read_obj(file):
    """
    Attempt to read from a .obj file and store the data into a dictionary.

    :keyword file The file to read.

    If the file is not readable an exception is thrown.
    The main reasons of an unreadable file are:
        -File doesn't exist (maybe you have misspelled it or the path is wrong)
        -Impossible to read the file (You or this program doesn't have permission to read from this file)
        -Format is incorrect.

        The format of the ply file must respect some rules that you can find here:
        http://bored-dev.org/toto

    :return dictionary A dictionary where are stored all the 3D data.
    """
    dictionary = dict()
    return dictionary

