"""
This module contains functions that will write 3D data into a specific file.

The 3D data are stored into a dictionary. The dictionary must follow this format:
    -blablabla
"""
import os


def write_ply(file, **dictionary):
    """Write a .ply file from data."""
    if os.path.exists(file):
        verteces = 0
        fw = open(file, 'w')
        fw.write('ply\n')
        fw.write('format ascii 1.0\n')
        fw.write('comment author: _Blaoi - 3DConverter\n')
        fw.write('property float x\n')
        fw.write('property float y\n')
        fw.write('property float z\n')
        fw.write('element vertex ' + str(verteces) + '\n')
        fw.write('end_header')
        fw.close()
    else:
        raise OSError(file + " doesn't exists !")


def write_obj(file, **dictionary):
    """Write a .obj file from data."""
